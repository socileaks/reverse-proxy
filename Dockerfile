FROM registry.gitlab.com/vivalerts/demos/nginx-geoip2
RUN apk add --no-cache certbot-nginx~=3.0.1
COPY init.sh /docker-entrypoint.d/
COPY flags.map /
COPY nginx.conf geoip2.conf /etc/nginx/
COPY default.conf /etc/nginx/conf.d/
COPY static /usr/share/nginx/html
